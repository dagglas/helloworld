package com.test;

public class UselessClass {
	public void uselessMethod() { }
	
	public void anotherUselessMethod() {
		System.out.println("I'm useless too!");
	}
}
